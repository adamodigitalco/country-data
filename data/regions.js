'use strict';

var regions = {};

regions.centralAsia = {
    name: 'Central Asia',
    countries: [
        // source is http://en.wikipedia.org/wiki/Central_Asia
        'TR', // Turkey
        'AF', // Afghanistan
        'CY', // Cyprus
        'KZ', // Kazakhstan
        'KG', // Kyrgyzstan
        'TJ', // Tajikistan
        'TM', // Turkmenistan
        'UZ', // Uzbekistan
    ]
}

regions.southernAsia = {
    name: 'Southern Asia',
    countries: [
        // source is http://en.wikipedia.org/wiki/South_Asia
        'BD', // Bangladesh
        'BT', // Bhutan
        'IN', // India
        'MV', // Maldives
        'NP', // Nepal
        'PK', // Pakistan
        'LK', // Sri Lanka
        'SY', // Seychelles
    ]
}

regions.southeastAsia = {
    name: 'Southeast Asia',
    countries: [
        // source is http://en.wikipedia.org/wiki/Southeast_Asia
        'BN', // Brunei
        'KH', // Cambodia
        'TL', // East Timor
        'ID', // Indonesia
        'LA', // Laos
        'MY', // Malaysia
        'MM', // Myanmar (Burma)
        'PH', // Philippines
        'SG', // Singapore
        'TH', // Thailand
        'VN', // Vietnam
    ]
}

regions.eastAsia = {
    name: 'East Asia',
    countries: [
        // source is http://en.wikipedia.org/wiki/East_Asia
        'CN', // China
        'HK', // Hong Kong
        'JP', // Japan
        'KP', // North Korea
        'KR', // South Korea
        'MO', // Macao
        'MN', // Mongolia
        'TW', // Taiwan
    ]
}

regions.westernAsia = {
    name: 'Western Asia',
    countries: [
        // source is http://en.wikipedia.org/wiki/Western_Asia
        'AM', // Armenia
        'AZ', // Azerbaijan
        'BH', // Bahrain
        'IR', // Iran
        'IQ', // Iraq
        'IL', // Israel
        'JO', // Jordan
        'KW', // Kuwait
        'LB', // Lebanon
        'OM', // Oman
        'PS', // Palestinian territories
        'QA', // Qatar
        'SA', // Saudi Arabia
        'SY', // Syria
        'AE', // United Arab Emirates
        'YE', // Yemen
    ]
}

regions.centralAfrica = {
    name: 'Central Aftrica',
    countries: [
        // source is http://en.wikipedia.org/wiki/Central_Africa
        'AO', // Angola
        'CM', // Cameroon
        'CF', // Central African Republic
        'TD', // Chad
        'CG', // Republic of the Congo
        'CD', // Democratic Republic of the Congo
        'GQ', // Equatorial Guinea
        'GA', // Gabon
        'ST', // São Tomé and Príncipe
        'RW', // Rwanda
        'BI', // Burundi
    ]
};

regions.northAfrica = {
    name: 'North Africa',
    countries: [
        // source is http://en.wikipedia.org/wiki/North_Africa
        'DZ', // Algeria
        'EG', // Egypt
        'LY', // Libya
        'MA', // Morocco
        'SD', // Sudan
        'TN', // Tunisia
        'EH', // Western Sahara
    ]
};

regions.southernAfrica = {
    name: 'Southern Africa',
    countries: [
        // source is http://en.wikipedia.org/wiki/Southern_Africa
        'BW', // Botswana
        'LS', // Lesotho
        'NA', // Namibia
        'ZA', // South Africa
        'SZ', // Swaziland
    ]
};

regions.eastAfrica = {
    name: 'East Africa',
    countries: [
        // source is http://en.wikipedia.org/wiki/East_Africa
        'BI', // Burundi
        'KM', // Comoros
        'DJ', // Djibouti
        'ER', // Eritrea
        'ET', // Ethiopia
        'KE', // Kenya
        'MG', // Madagascar
        'MW', // Malawi
        'MU', // Mauritius
        'YT', // Mayotte (France)
        'MZ', // Mozambique
        'RE', // Réunion (France)
        'RW', // Rwanda
        'SC', // Seychelles
        'SO', // Somalia
        'SS', // South Sudan
        'TZ', // Tanzania
        'UG', // Uganda
        'ZM', // Zambia
        'ZW', // Zimbabwe
    ]
};

regions.westAfrica = {
    name: 'West Africa',
    countries: [
        // source is http://en.wikipedia.org/wiki/West_Africa
        'BJ', // Benin
        'BF', // Burkina Faso
        'CV', // Cape Verde
        'CI', // Côte d'Ivoire
        'GM', // Gambia
        'GH', // Ghana
        'GN', // Guinea
        'GW', // Guinea-Bissau
        'LR', // Liberia
        'ML', // Mali
        'MR', // Mauritania
        'NE', // Niger
        'NG', // Nigeria
        'SN', // Senegal
        'SL', // Sierra Leone
        'TG', // Togo
    ]
};

regions.centralAmerica = {
    name: 'Central America',
    countries: [
        // source is http://en.wikipedia.org/wiki/Central_America
        'BZ', // Belize
        'CR', // Costa Rica
        'SV', // El Salvador
        'GT', // Guatemala
        'HN', // Honduras
        'NI', // Nicaragua
    ]
}

regions.northernAmerica = {
    name: 'Northern America',
    countries: [
        // source is http://en.wikipedia.org/wiki/Northern_America
        'CA', // Canada
        'US', // United States
        'MX', // Mexico
    ]
}

regions.caribbean = {
    name: 'Caribbean',
    countries: [
        // source is http://en.wikipedia.org/wiki/Caribbean
        'AI', // Anguilla
        'AG', // Antigua and Barbuda
        'AW', // Aruba
        'BS', // Bahamas
        'BB', // Barbados
        'BQ', // Bonaire, Sint Eustatius & Saba
        'VG', // British Virgin Islands
        'KY', // Cayman Islands
        'CU', // Cuba
        'CW', // Curaçao
        'DM', // Dominica
        'DO', // Dominican Republic
        'GD', // Grenada
        'GP', // Guadeloupe
        'HT', // Haiti
        'JM', // Jamaica
        'MQ', // Martinique
        'MS', // Montserrat
        'PR', // Puerto Rico
        'BL', // Saint Barthélemy
        'KN', // St. Kitts & Nevis
        'LC', // Saint Lucia
        'MF', // Saint Martin
        'VC', // Saint Vincent and the Grenadines
        'SX', // Sint Maarten
        'TT', // Trinidad and Tobago
        'TC', // Turks & Caicos
        'VI', // United States Virgin Islands
    ]
}

regions.southAmerica = {
    name: 'South America',
    countries: [
        // source is http://en.wikipedia.org/wiki/South_America
        'AR', // Argentina
        'BO', // Bolivia
        'BR', // Brazil
        'CL', // Chile
        'CO', // Colombia
        'EC', // Ecuador
        'FK', // Falkland Islands
        'GF', // French Guiana
        'GY', // Guyana
        'PA', // Panama
        'PY', // Paraguay
        'PE', // Peru
        'SR', // Suriname
        'UY', // Uruguay
        'VE', // Venezuela
    ]
}

regions.westernEurope = {
    name: 'Western Europe',
    countries: [
        // source is http://en.wikipedia.org/wiki/Western_Europe
        'AT', // Austria
        'BE', // Belgium
        'FR', // France
        'DE', // Germany
        'LI', // Liechtenstein
        'LU', // Luxembourg
        'MC', // Monaco
        'NL', // Netherlands
        'CH', // Switzerland
        'AD', // Andorra
        'DK', // Denmark
        'FO', // Faroe Islands
        'FI', // Finland
        'GI', // Gibraltar
        'GR', // Greece
        'GL', // Greenland
        'IT', // Italy
        'GG', // Guernsey
        'IS', // Iceland
        'MT', // Malta
        'IE', // Republic of Ireland
        'NO', // Norway
        'PT', // Portugal
        'ES', // Spain
        'SE', // Sweden
        'TR', // Turkey
        'GB', // United Kingdom
    ]
}

regions.southernEurope = {
    name: 'Southern Europe',
    countries: [
        // source is http://en.wikipedia.org/wiki/Southern_Europe
        'AL', // Albania
        'BA', // Bosnia and Herzegovina
        'BG', // Bulgaria
        'MK', // Republic of Macedonia
        'ME', // Montenegro
        'RS', // Serbia
        'RO', // Romania
        'XK', // Kosovo
    ]
}

regions.easternEurope = {
    name: 'Eastern Europe',
    countries: [
        // source is http://en.wikipedia.org/wiki/Eastern_Europe
        'EE', // Estonia
        'BY', // Belarus
        'BG', // Bulgaria
        'CZ', // Czech Republic
        'HR', // Croatia
        'GE', // Georgia
        'AM', // Armenia
        'AZ', // Azerbaijan
        'HU', // Hungary
        'LV', // Latvia
        'LT', // Lithuania
        'MD', // Moldova
        'PL', // Poland
        'RU', // Russia
        'SK', // Slovakia
        'SI', // Slovenia
        'UA', // Ukraine
    ]
}

regions.australia = {
    name: 'Australia',
    countries: [
        // source is http://en.wikipedia.org/wiki/Australasia
        'AU', // Australia
        'PG', // Papua New Guinea
        'NF', // Norfolk Island
        'NZ', // New Zealand
        'FJ', // Fiji
        'SB', // Solomon Islands
        'TV', // Tuvalu
        'WS', // Samoa
        'TO', // Tonga
    ]
};

regions.russia = {
    name: 'Russia',
    countries: [
        // source is http://en.wikipedia.org/wiki/Russia
        'RU', // Russia

    ]
};
module.exports = regions;